> *This guide is made to help you solve various errors that may occur during the whole procedure: from building the app to running it and bundling it. Feel free to ask any questions in our community on [Gitter](https://gitter.im/AOSSIE/CrowdAlert "AOSSIE Gitter Community")*

---

#### 1. App crashing on running react-native run-android

Check that your `.env` file is setup properly and has the keys to run. Look in key.md file to configure the keys properly.

#### 2. Error screen on app with message: null is not an object (evaluating this.props.user.email)

__*Why this error?*__

*When you configure your project in firebase, by default it sets read and write access to false, so that external users don't modify the content of the database. Thus when react-native fetches the user profile, it gets empty response(null object) and has no properties.*

__*Solution*__

1. Go to your project's firebase console.
2. From the left hand panel, select 'Database', then select 'Realtime Database'
3. Click on 'Rules' tab.
4. Change read and write values from `false` to `true` under rules {}.
5. Click on 'Publish' to save your changes. Build and run project again.


#### 3. How to build a serverless/debug APK?

1. Navigate to project root directory and open a terminal.
2. Run the below command to start the node package server localhost. This also resets the cache by adding the flag at the end.  
`npm start -- --reset-cache`  
> *Make sure you already ran npm install before this so that node packages are also bundled otherwise it gives an error.*  

3. After it shows, 'Loading dependency graph, done', open a new terminal and type in:  
`mkdir -p android/app/src/main/assets`  
This creates a directory to store the assets to bundle into the app.  

4. Now run the following command:  
`curl "http://localhost:8081/index.bundle?platform=android" -o "android/app/src/main/assets/index.bundle"`  
This will collect all your assets to the assets folder you created before and pack them in index.bundle.  

> *You can see the progress for this on the npm tab where it is actually running to bundle the files starting from index.js*  

5. After curl downloads all the files, run the following command to bundle those assets from index.android you created before:  
`react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res`  

6. Finally, run the following comand to build the react app:  
`react-native run-android`  

When, it completes, you will see 'BUILD SUCCESSFUL' in the console, your apk will be present in the folder:  
> *<project_dir\>/android/app/build/outputs/apk/debug*  

Look for the file *app-debug.apk*. This will be the serverless/debug APK.  

### *__Note__*
> If any of your command fails due to error showing persmission denied, use `sudo` before the commands and try again.

#### 4. 'Fix Search Paths/Argument List too long' in iOS build process  

Refer to [this pull](https://github.com/react-native-community/react-native-google-signin/pull/333/files) for the fix.  

#### 5. Build/Compilation Error: '...compileDebugJavaWithJavac'

This error occurs if you use Java version above 8. Currently react-native supports JDK 8.  
The only current workaround through this problem is to downgrade/upgrade JDK to version 8. If you have Java version below/above 8, then you might want to upgrade it to 8. To check java version on your laptop:  
 `java -version`

For hastle free work, we strongly recommend installing OpenJDK v1.8.x.  
You can also download JDK from [here](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).  

#### 6. Issues with react-native-maps/ Map not showing some places on device  

If at any moment using the app, the maps stop working or does not show places, it might be due to two reasons:  
1. *Google Maps API key not set up* : Check that you have set up the Google Maps API key in `.env` file. For any help regarding setting up of keys, see the [keys installation guide](https://gitlab.com/aossie/CrowdAlert-Mobile/blob/master/doc/keys.md).

2. If the maps still don't show up, refer to the following [official documentation of react-native-maps here](https://github.com/react-native-community/react-native-maps/blob/master/docs/installation.md) for any support.